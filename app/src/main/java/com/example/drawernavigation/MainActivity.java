package com.example.drawernavigation;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout drawer;
    private  Toolbar toolbar;
    private  NavigationView navigationView;
    private ActionBarDrawerToggle toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toobar);
        drawer  = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        init();

//        if(savedInstanceState == null) {
//
//            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
//                    new MessageFragment()).commit();
//            navigationView.setCheckedItem(R.id.nav_message);
//
//        }

    }

    private void init(){
        setValuesToobar();
        setClickNavigationView();
        setDrawerToggle();
    }

    private void setDrawerToggle() {

        toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    private void setClickNavigationView() {
        navigationView.setNavigationItemSelectedListener(clickItemNavition());
    }

    private void setValuesToobar() {
        setSupportActionBar(toolbar);
    }

    private NavigationView.OnNavigationItemSelectedListener clickItemNavition() {
        return new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {

                switch (item.getItemId()){

                    case R.id.nav_message:

                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                                new MessageFragment()).commit();

                        break;

                    case R.id.nav_chat:

                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                                new ChatFragment()).commit();
                        break;
                }

                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        };
    }


    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START)){

            drawer.closeDrawer(GravityCompat.START);
        }else{
            super.onBackPressed();
        }

    }
}
